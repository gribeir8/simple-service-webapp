package com.example;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;

/**
 * Root resource (exposed at "myresource" path)
 */
@Path("myresource")
public class MyResource {

	public static final String TRASH = ")]}',";
	public static final String PROVIDER_ENDPOINT = "https://www.ing.nl/api/locator/atms/";
	
    /**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     * @throws Exception 
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public JsonNode getIt(@QueryParam("city") String city) throws Exception {
    	
    	String json = JsonClient.sendGet(PROVIDER_ENDPOINT);
    	//payload starts with dirt
		json = json.substring(json.indexOf(TRASH)+TRASH.length());
    	
    	StringBuilder criteria = new StringBuilder();
    	if(!StringUtils.isEmpty(city)){
        	//on all element look for city (regex ignore case)
        	criteria.append("$.*[?(@.address.city =~ /").append(city).append("/i)]");
    	}else{
        	//on all element look for city (regex ignore case)
        	criteria.append("$.*");
    	}
    	List<String> list = JsonPath.read(json, criteria.toString());

    	//parses string to json objects
    	ObjectMapper mapper = new ObjectMapper();
    	JsonNode node = mapper.readTree(list.toString());
		return node;
    }
}
