package com.example;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.jayway.jsonpath.JsonPath;

public class JsonClient {

	public static final String TRASH = ")]}',";
	public static final String PROVIDER_ENDPOINT = "https://www.ing.nl/api/locator/atms/";
	
	public static void main(String[] args){
		
		try {
			String json = JsonClient.sendGet(PROVIDER_ENDPOINT);
			json = json.substring(json.indexOf(TRASH)+TRASH.length());
			System.out.println("json: " + json);

			String city = "AMSTERDAM";
	    	StringBuilder criteria = new StringBuilder();
	    	criteria.append("$.*[?(@.address.city =~ /").append(city).append("/i)]");
	    	
	    	System.out.println("criteria: "+criteria.toString());

	    	List<String> list = JsonPath.read(json, criteria.toString());
			
	    	ObjectMapper mapper = new ObjectMapper();
	    	JsonNode node = mapper.readTree(list.toString());

			System.out.println("json: " +node);
		} catch (Exception e) {

			e.printStackTrace();
		}
		
	}
	
	// HTTP GET request
	public static String sendGet(String url) throws Exception {

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		int responseCode = con.getResponseCode();
//		System.out.println("\nSending 'GET' request to URL : " + url);
//		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		return response.toString();

	}
	
}
